//
//  StootViewController.swift
//  Stoot
//
//  Created by Jessica Alula on 08/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import UIKit

final class StootViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    private var presenter: StootPresenter!

    init(withPresenter presenter: StootPresenter) {
        self.presenter = presenter
        super.init(nibName: "StootViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        self.presenter = nil
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        bindPresenter()
        presenter?.getStootDetails()
    }
        
    private func registerCells() {
        tableView?.register(StootListTableViewCell.nib, forCellReuseIdentifier: StootListTableViewCell.reuseIdentifier)
        tableView?.register(StootQuestionTableViewCell.nib, forCellReuseIdentifier: StootQuestionTableViewCell.reuseIdentifier)
        tableView?.register(StootLocationTableViewCell.nib, forCellReuseIdentifier: StootLocationTableViewCell.reuseIdentifier)
        tableView?.register(StootHeaderTableViewCell.nib, forHeaderFooterViewReuseIdentifier: StootHeaderTableViewCell.reuseIdentifier)
    }
        
    private func bindPresenter() {
        presenter?.retrievedStoot = { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.tableView?.reloadData()
            }
        }
    }
    
    private func stootListTableViewCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: StootListTableViewCell.reuseIdentifier, for: indexPath) as? StootListTableViewCell
        else { fatalError("Failed to return StootListTableViewCell") }
        if let stootDetail = presenter?.stootDetail {
            cell.configureStootCell(stootDetail)
        }
        return cell
    }
    
    private func stootLocationTableViewCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: StootLocationTableViewCell.reuseIdentifier, for: indexPath) as? StootLocationTableViewCell
        else { fatalError("Failed to return StootLocationTableViewCell") }
        if let mapPointCoordinate = presenter?.mapPointCoordinate {
            cell.configureMapPointCoordinate(mapPointCoordinate)
        }
        return cell
    }
    
    private func stootQuestionTableViewCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: StootQuestionTableViewCell.reuseIdentifier, for: indexPath) as? StootQuestionTableViewCell
        else { fatalError("Failed to return StootQuestionTableViewCell") }
        if let questions = presenter?.questions,
            questions.indices.contains(indexPath.item) {
            cell.configureWithQuestion(questions[indexPath.item])
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension StootViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let stootDetailSection = StootDetailSection(rawValue: section),
            stootDetailSection.hasAHeader,
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: StootHeaderTableViewCell.reuseIdentifier) as? StootHeaderTableViewCell,
            let title = stootDetailSection.title
        else { return nil }
        header.configureWithTitle(title)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let stootDetailSection = StootDetailSection(rawValue: section) else { return 0 }
        return stootDetailSection.heightForHeader
    }
}

// MARK: - UITableViewDataSource
extension StootViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter?.numberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfRowsInSection(section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let stootDetailSection = StootDetailSection(rawValue: indexPath.section) else {
            fatalError("Unknowned indexPath")
        }
        switch stootDetailSection {
        case .stootDatas:
            return stootListTableViewCell(tableView, cellForRowAt: indexPath)
        case .map:
            return stootLocationTableViewCell(tableView, cellForRowAt: indexPath)
        case .questions:
            return stootQuestionTableViewCell(tableView, cellForRowAt: indexPath)
        }
    }
}
