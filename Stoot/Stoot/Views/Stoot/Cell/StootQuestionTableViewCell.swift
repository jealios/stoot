//
//  StootQuestionTableViewCell.swift
//  Stoot
//
//  Created by Jessica Alula on 08/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import UIKit

final class StootQuestionTableViewCell: UITableViewCell {
    @IBOutlet private weak var questionLabel: UILabel!
    @IBOutlet private weak var answerLabel: UILabel!
    
    static let nib = UINib(nibName: "StootQuestionTableViewCell", bundle: nil)
    static let reuseIdentifier = "StootQuestionTableViewCell"
    
    /// Configures the cell with the couple question/answer that holds stoot's precisions
    /// - Parameter question: a Question type var that holds key informations
    func configureWithQuestion(_ question: Question) {
        questionLabel?.text = question.questionTitle
        answerLabel?.text = question.answer?.text ?? "-"
    }
    
}
