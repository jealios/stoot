//
//  StootLocationTableViewCell.swift
//  Stoot
//
//  Created by Jessica Alula on 08/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import UIKit
import MapKit

final class StootLocationTableViewCell: UITableViewCell {
    @IBOutlet private weak var mapView: MKMapView!
    
    static let nib = UINib(nibName: "StootLocationTableViewCell", bundle: nil)
    static let reuseIdentifier = "StootLocationTableViewCell"
    
    /// This cell contains a map that displays a pin on the stoot's location
    /// - Parameter coordinate: a CLLocationCoordinate2D storing latitude and longitude of the stoot's location
    func configureMapPointCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)
        mapView.setCenter(coordinate, animated: true)
        guard let distance = CLLocationDistance(exactly: 20000) else { return }
        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: distance, longitudinalMeters: distance)
        mapView.setRegion(region, animated: true)
    }
    
}
