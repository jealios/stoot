//
//  StootHeaderTableViewCell.swift
//  Stoot
//
//  Created by Jessica Alula on 08/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import UIKit

final class StootHeaderTableViewCell: UITableViewHeaderFooterView {
    @IBOutlet private weak var titleLabel: UILabel!
    
    static let nib = UINib(nibName: "StootHeaderTableViewCell", bundle: nil)
    static let reuseIdentifier = "StootHeaderTableViewCell"
    
    func configureWithTitle(_ title: String) {
        titleLabel?.text = title
    }
    
}
