//
//  StootListTableViewCell.swift
//  Stoot
//
//  Created by Jessica Alula on 07/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

final class StootListTableViewCell: UITableViewCell {
    @IBOutlet private weak var userPictureImageView: UIImageView!
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var stootTitleLabel: UILabel!
    @IBOutlet private weak var stootTimeIntervalLabel: UILabel!
    @IBOutlet private weak var stootAddressLabel: UILabel!
    
    static let nib = UINib(nibName: "StootListTableViewCell", bundle: nil)
    static let reuseIdentifier = "StootListTableViewCell"
    
    private func addPictureWithURLFromUser(_ user: User) {
        guard let pictureURL = user.pictureURL,
            let placeholderImage = UIImage(named: "placeholderImage")
        else { return }
        userPictureImageView?.af.setImage(withURL: pictureURL, cacheKey: user.name, placeholderImage: placeholderImage, serializer: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.noTransition, runImageTransitionIfCached: false, completion: nil)
        userPictureImageView?.contentMode = .scaleAspectFill
    }
    
    /// Configures the cell with informations regarding user and the stoot
    /// - Parameter stootDatas: a StootCellDatas type var that holds datas to display
    func configureStootCell(_ stootDatas: StootCellDatas) {
        let stoot = stootDatas.stoot
        userNameLabel?.text = stoot.user.name
        stootTitleLabel?.text = stoot.title
        stootAddressLabel?.text = stoot.address
        stootTimeIntervalLabel?.text = stootDatas.displayedFromDetail ? stoot.formattedCreationDate : stoot.creationDateInterval
        addPictureWithURLFromUser(stootDatas.stoot.user)
    }
}
