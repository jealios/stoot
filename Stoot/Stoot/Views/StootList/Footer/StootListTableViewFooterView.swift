//
//  StootListTableViewFooterView.swift
//  Stoot
//
//  Created by Jessica Alula on 07/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import UIKit

protocol StootListTableViewFooterViewDelegate: class {
    func stootListTableViewFooterViewSeePreviousPage(_ stootListTableViewFooterView: StootListTableViewFooterView)
    func stootListTableViewFooterViewSeeNextPage(_ stootListTableViewFooterView: StootListTableViewFooterView)
}

final class StootListTableViewFooterView: UITableViewHeaderFooterView {
    @IBOutlet private weak var previousButton: UIButton!
    @IBOutlet private weak var nextButton: UIButton!
    @IBOutlet private weak var currentPageIndexLabel: UILabel!
    @IBOutlet private weak var resultsCountLabel: UILabel!
    private weak var delegate: StootListTableViewFooterViewDelegate?
    
    static let nib = UINib(nibName: "StootListTableViewFooterView", bundle: nil)
    static let reuseIdentifier = "StootListTableViewFooterView"
    
    /// The footer displays 2 buttons (previous & next) and the total of stoots available
    /// - Parameters:
    ///   - pagination: a Pagination type var that holds pagination (previous/current/next & total)
    ///   - delegate: a StootListTableViewFooterViewDelegate type var that manages buttons interactions
    func configureWithPagination(_ pagination: Pagination, andDelegate delegate: StootListTableViewFooterViewDelegate) {
        previousButton.isEnabled = pagination.previousPage != nil
        nextButton.isEnabled = pagination.nextPage != nil
        currentPageIndexLabel.text = "\(pagination.currentPage)"
        resultsCountLabel.text = "\(pagination.total) résultat(s)"
        self.delegate = delegate
    }
    
    @IBAction private func seePreviousPage(_ sender: UIButton) {
        delegate?.stootListTableViewFooterViewSeePreviousPage(self)
    }
    
    @IBAction private func seeNextPage(_ sender: UIButton) {
        delegate?.stootListTableViewFooterViewSeeNextPage(self)
    }
    
}
