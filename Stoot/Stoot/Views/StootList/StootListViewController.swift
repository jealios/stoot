//
//  StootListViewController.swift
//  Stoot
//
//  Created by Jessica Alula on 07/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import UIKit

final class StootListViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    private var presenter: StootListPresenter!
    
    init(withPresenter presenter: StootListPresenter) {
        self.presenter = presenter
        super.init(nibName: "StootListViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        self.presenter = nil
        super.init(coder: coder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        bindPresenter()
        presenter?.retrieveInitialList()
        title = "Stoots"
    }
    
    private func registerCells() {
        tableView?.register(StootListTableViewCell.nib, forCellReuseIdentifier: StootListTableViewCell.reuseIdentifier)
        tableView?.register(StootListTableViewFooterView.nib, forHeaderFooterViewReuseIdentifier: StootListTableViewFooterView.reuseIdentifier)
    }
    
    private func bindPresenter() {
        presenter?.updatedList = { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.tableView?.reloadData()
                let topIndexPath = IndexPath(row: 0, section: 0)
                self.tableView?.scrollToRow(at: topIndexPath, at: .top, animated: true)
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension StootListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: StootListTableViewCell.reuseIdentifier, for: indexPath) as? StootListTableViewCell
        else { fatalError("Failed to return StootListTableViewCell") }
        
        if let stootCellDatas = presenter?.stootCellDatas(atIndexPath: indexPath) {
            cell.configureStootCell(stootCellDatas)
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension StootListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let stootCellDatas = presenter?.stootCellDatas(atIndexPath: indexPath) else { return }
        let stootPresenter = StootPresenter(withStootId: stootCellDatas.stoot.id)
        let stootViewController = StootViewController(withPresenter: stootPresenter)
        navigationController?.pushViewController(stootViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: StootListTableViewFooterView.reuseIdentifier) as? StootListTableViewFooterView
            else { fatalError("Failed to return StootListTableViewFooterView") }
        
        if let pagination = presenter?.pagination {
            footer.configureWithPagination(pagination, andDelegate: self)
        }
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 90
    }
}

// MARK: - StootListTableViewFooterViewDelegate
extension StootListViewController: StootListTableViewFooterViewDelegate {
    func stootListTableViewFooterViewSeePreviousPage(_ stootListTableViewFooterView: StootListTableViewFooterView) {
        presenter?.retrievePreviousList()
    }
    
    func stootListTableViewFooterViewSeeNextPage(_ stootListTableViewFooterView: StootListTableViewFooterView) {
        presenter?.retrieveNextList()
    }
}
