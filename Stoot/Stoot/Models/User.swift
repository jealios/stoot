//
//  User.swift
//  Stoot
//
//  Created by Jessica Alula on 06/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

struct User: Codable {
    let name: String
    let pictureURL: URL?
    let ratingCount: Int
    
    enum CodingKeys: String, CodingKey {
        case name
        case pictureURL = "profile_picture_url"
        case ratingCount = "eval_count"
    }
}
