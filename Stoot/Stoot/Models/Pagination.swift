//
//  Pagination.swift
//  Stoot
//
//  Created by Jessica Alula on 07/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

struct Pagination: Codable {
    let previousPage: Int?
    let nextPage: Int?
    let currentPage: Int
    let perPage: Int
    let total: Int
    
    enum CodingKeys: String, CodingKey {
        case previousPage = "previous_page"
        case nextPage = "next_page"
        case currentPage = "current_page"
        case perPage = "per_page"
        case total
    }
}

