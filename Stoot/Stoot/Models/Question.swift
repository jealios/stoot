//
//  Question.swift
//  Stoot
//
//  Created by Jessica Alula on 08/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

struct Question: Codable {
    let questionTitle: String
    let answer: Answer?
    
    enum CodingKeys: String, CodingKey {
        case answer
        case questionTitle = "question_title"
    }
}
