//
//  StootList.swift
//  Stoot
//
//  Created by Jessica Alula on 07/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

struct StootList: Codable {
    let pagination: Pagination
    let collection: [Stoot]
}
