//
//  Answer.swift
//  Stoot
//
//  Created by Jessica Alula on 08/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

struct Answer: Codable {
    private let type: String
    private let value: String?
    private let choices: [Choice]?
    private let to: AnswerTo?
    
    var text: String? {
        guard let answerType = AnswerType(rawValue: type) else { return nil }
        switch answerType {
        case .text,
             .date:
            return value
        case .location:
            return to?.address
        case .singleChoice:
            return choices?.first?.value
        case .multipleChoices:
            return choices?.compactMap({ return $0.value }).joined(separator: "\n")
        case .photos:
            return "Photo"
        }
    }
}
