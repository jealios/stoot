//
//  Choice.swift
//  Stoot
//
//  Created by Jessica Alula on 08/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

struct Choice: Codable {
    let value: String
}
