//
//  AnswerType.swift
//  Stoot
//
//  Created by Jessica Alula on 08/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

enum AnswerType: String {
    case text
    case singleChoice = "single_choice"
    case location
    case multipleChoices = "multiple_choices"
    case photos
    case date
}
