//
//  StootDetailSection.swift
//  Stoot
//
//  Created by Jessica Alula on 08/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation
import UIKit

enum StootDetailSection: Int, CaseIterable {
    case stootDatas = 0
    case map
    case questions
    
    var title: String? {
        switch self {
        case .stootDatas:
            return nil
        case .map:
            return "Localisation"
        case .questions:
            return "Informations clés"
        }
    }
    
    var heightForHeader: CGFloat {
        switch self {
        case .stootDatas:
            return 0
        case .map,
             .questions:
            return 90
        }
    }
    
    var hasAHeader: Bool {
        switch self {
        case .stootDatas:
            return false
        case .map,
             .questions:
            return true
        }
    }
}
