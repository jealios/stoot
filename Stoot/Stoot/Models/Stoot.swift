//
//  Stoot.swift
//  Stoot
//
//  Created by Jessica Alula on 06/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

struct Stoot: Codable {
    let id: Int
    let user: User
    let title: String
    let address: String
    let positionLatitude: String
    let positionLongitude: String
    let answerWizard: AnswerWizard?
    private let creationDate: String
    var creationDateInterval: String? {
        guard let interval = NSCalendar.dateIntervalDescription(creationDate)
            else { return nil }
        return "il y à \(interval)"
    }
    var formattedCreationDate: String? {
        return NSCalendar.dayMonthYearDateDescription(creationDate)
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case user
        case title
        case address
        case creationDate = "created_at"
        case positionLatitude = "lat"
        case positionLongitude = "lng"
        case answerWizard = "answer_wizard"
    }
}
