//
//  DataManager.swift
//  Stoot
//
//  Created by Jessica Alula on 06/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

public class DataManager {
    
    /// Returns an URLRequest that is configured with required headers and has proper httpMethod set
    /// - Parameters:
    ///   - url: the URL to create URLRequest from
    ///   - method: the method to use to process the request
    ///   - httpBody: any data required in the request body
    class func urlRequestWithHeaders(_ url: URL, withMethod method: Webservice.HTTPMethod, withHttpBody httpBody: Data?) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.setValue(Webservice.Header.acceptVersion.value, forHTTPHeaderField: Webservice.Header.acceptVersion.key)
        urlRequest.setValue(Webservice.Header.requestId.value, forHTTPHeaderField: Webservice.Header.requestId.key)
        urlRequest.httpMethod = method.rawValue
        urlRequest.httpBody = httpBody
        return urlRequest
    }
    
    /// Call to webservice to retrieve a page of stoots
    /// - Parameters:
    ///   - index: the index of the page that will be retrieved
    ///   - result: a completion handler that will manage either the retrieved stootList or an error
    class func getStootListPage(_ index: Int, result: @escaping (Result<StootList, Error>) -> Void) {
        guard let url = URL(string: Webservice.Path.GetStootList) else { return }
        let body = "page=\(index)"
        let data = body.data(using: String.Encoding.utf8)
        let urlRequest = DataManager.urlRequestWithHeaders(url, withMethod: Webservice.HTTPMethod.POST, withHttpBody: data)
        Webservice.processRequest(urlRequest, completion: result)
    }
    
    /// Call to webservice to retrieve given stoot by its id
    /// - Parameters:
    ///   - id: the id of the stoot to retrieve
    ///   - result: a completion handler that will manage either the retrieved stoot or an error
    class func getStoot(withId id: Int, result: @escaping (Result<Stoot, Error>) -> Void) {
        let stootPath = String(format: Webservice.Path.GetStoot, arguments: [id])
        guard let url = URL(string: stootPath) else { return }
        let urlRequest = DataManager.urlRequestWithHeaders(url, withMethod: Webservice.HTTPMethod.GET, withHttpBody: nil)
        Webservice.processRequest(urlRequest, completion: result)
    }
    
}
