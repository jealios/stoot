//
//  URLSession+ApplicationSpecific.swift
//  Stoot
//
//  Created by Jessica Alula on 06/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

extension URLSession {
    func dataTask(with urlRequest: URLRequest, result: @escaping (Result<(URLResponse, Data), Error>) -> Void) -> URLSessionDataTask {
        return dataTask(with: urlRequest) { (data, response, error) in
            if
                let error = error {
                result(.failure(error))
                
                return
                
            }
            guard let response = response, let data = data else {
                
                let error = NSError(domain: "error", code: 0, userInfo: nil)
                result(.failure(error))
                
                return
                
            }
            result(.success((response, data)))
        }
    }
}
