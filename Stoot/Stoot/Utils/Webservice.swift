//
//  Webservice.swift
//  Stoot
//
//  Created by Jessica Alula on 06/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

// MARK: - Webservice.Path extension
extension Webservice.Path {
    static let GetStootList = "https://bff-mobile-dev.stootie.com/demands/filter"
    static let GetStoot = "https://bff-mobile-dev.stootie.com/demands/%d/details"
}

// MARK: - Webservice.Header extension
extension Webservice.Header {
    static let acceptVersion: (key: String, value: String) = (key: "Accept-Version", value: "3.0.0")
    static let requestId: (key: String, value: String) = (key: "X-Request-Id", value: UUID().uuidString)
}

// MARK: - Webservice class
public class Webservice {
    public struct Path {}
    public struct Header {}
    public enum HTTPMethod: String {
        case GET
        case POST
    }
    
    /// Process a request that will retrieve a type <T> decodable model
    /// - Parameters:
    ///   - urlRequest: the URLRequest to process
    ///   - completion: the completion handler that will manage the result
    class func processRequest<T: Decodable>(_ urlRequest: URLRequest, completion: @escaping (Result<T, Error>) -> Void) {
        URLSession.shared.dataTask(with: urlRequest) { (result) in
            switch result {
            case .success(let (response, data)):
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode, 200..<299 ~= statusCode else {
                    let error = NSError(domain: "error", code: 0, userInfo: nil)
                    completion(.failure(error))
                    return
                }
                do {
                    let values = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(values))
                }
                catch {
                    let error = NSError(domain: "error", code: 0, userInfo: nil)
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
            }.resume()
    }
}
