//
//  NSCalendar+ApplicationSpecific.swift
//  Stoot
//
//  Created by Jessica Alula on 07/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

extension NSCalendar {
    static var currentCalendar: Calendar {
        var calendar = NSCalendar.current
        calendar.timeZone = NSTimeZone.system
        calendar.locale = Locale(identifier: "fr")
        return calendar
    }
    static var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = currentCalendar.timeZone
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return dateFormatter
    }
    
    /// Returns a string holding the greater time interval since a given date
    /// - Parameter startDate: the start date of the interval
    class func dateIntervalDescription(_ startDate: String) -> String? {
        guard let startDate = dateFormatter.date(from: startDate) else { return nil }
        
        let formatter = DateComponentsFormatter()
        formatter.maximumUnitCount = 1
        formatter.unitsStyle = .short
        formatter.calendar = currentCalendar
        formatter.allowedUnits = [.minute, .hour, .day, .weekOfMonth, .month, .year]
        return formatter.string(from: startDate, to: Date())
    }
    
    /// Returns a formatted date description with the day-month-year
    /// - Parameter date: a string holding a date
    class func dayMonthYearDateDescription(_ date: String) -> String? {
        guard let creationDate = dateFormatter.date(from: date) else { return nil }
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return formatter.string(from: creationDate)
    }
}
