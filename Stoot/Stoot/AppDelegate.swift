//
//  AppDelegate.swift
//  Stoot
//
//  Created by Jessica Alula on 06/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let presenter = StootListPresenter()
        let rootViewController = StootListViewController(withPresenter: presenter)
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.navigationBar.tintColor = .orange
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }
}

