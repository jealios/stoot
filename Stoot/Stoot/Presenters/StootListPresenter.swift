//
//  StootListPresenter.swift
//  Stoot
//
//  Created by Jessica Alula on 07/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation

final class StootListPresenter {
    
    // MARK: - Public vars
    var list: [Stoot]? {
        guard retrievedStootLists.indices.contains(retrievedStootListsIndex)
        else { return nil }
        return retrievedStootLists[retrievedStootListsIndex].collection
    }
    var pagination: Pagination? {
        guard retrievedStootLists.indices.contains(retrievedStootListsIndex)
            else { return nil }
        return retrievedStootLists[retrievedStootListsIndex].pagination
    }
    
    // MARK: - Public closure
    var updatedList: (() -> Void)?
    
    // MARK: - Private vars
    private var retrievedStootLists: [StootList] = []
    private var retrievedStootListsIndex = 0
    private var nextPageIndex: Int? {
        guard retrievedStootLists.indices.contains(retrievedStootListsIndex) else { return nil }
        return retrievedStootLists[retrievedStootListsIndex].pagination.nextPage
    }
    private var previousPageIndex: Int? {
        guard retrievedStootLists.indices.contains(retrievedStootListsIndex) else { return nil }
        return retrievedStootLists[retrievedStootListsIndex].pagination.previousPage
    }
    
    // MARK: - Public methods
    /// Returns a StootCellDatas type var that holds the stoot infromations and wether or not the cell will be displayed from detail
    /// - Parameter indexPath: the indexPath of the stoot
    func stootCellDatas(atIndexPath indexPath: IndexPath) -> StootCellDatas? {
        let itemIndex = indexPath.item
        guard let stootLists = list,
            stootLists.indices.contains(itemIndex)
        else { return nil }
        return StootCellDatas(stoot: stootLists[itemIndex], displayedFromDetail: false)
    }
    
    func retrieveInitialList() {
        getStootListPage(1)
    }
    
    func retrieveNextList() {
        guard let nextPageIndex = nextPageIndex else { return }
        if retrievedStootLists.indices.contains(nextPageIndex) {
            retrievedStootListsIndex += 1
            updatedList?()
        } else {
            getStootListPage(nextPageIndex)
        }
    }
    
    func retrievePreviousList() {
        guard let previousPageIndex = previousPageIndex else { return }
        if retrievedStootLists.indices.contains(previousPageIndex) {
            retrievedStootListsIndex -= 1
            updatedList?()
        } else {
            getStootListPage(previousPageIndex)
        }
    }
    
// MARK: - Private methods
    private func getStootListPage(_ pageIndex: Int) {
        DataManager.getStootListPage(pageIndex) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let stootList):
                self.retrievedStootLists.append(stootList)
                self.retrievedStootListsIndex = stootList.pagination.previousPage ?? 0
                self.updatedList?()
            case .failure: break
//                If i had more time i would have implemented the error part with alertViewControllers
            }
        }
    }
}

