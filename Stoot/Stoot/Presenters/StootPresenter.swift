//
//  StootPresenter.swift
//  Stoot
//
//  Created by Jessica Alula on 08/03/2020.
//  Copyright © 2020 jealios. All rights reserved.
//

import Foundation
import MapKit

class StootPresenter {
    
// MARK: - Public vars
    var mapPointCoordinate: CLLocationCoordinate2D? {
        guard let lat = stoot?.positionLatitude,
            let lng = stoot?.positionLongitude,
            let latDegrees = CLLocationDegrees(lat),
            let lngDegrees = CLLocationDegrees(lng)
            else { return nil }
        return CLLocationCoordinate2D(latitude: latDegrees, longitude: lngDegrees)
    }
    var numberOfSections: Int {
        guard let questions = questions,
            questions.count > 0
            else { return StootDetailSection.allCases.count - 1 }
        return StootDetailSection.allCases.count
    }
    var questions: [Question]? {
        guard let stoot = stoot,
            let questions = stoot.answerWizard?.wizard.questions
        else { return nil }
        return questions
    }
    var stootDetail: StootCellDatas? {
        guard let stoot = stoot else { return nil }
        return StootCellDatas(stoot: stoot, displayedFromDetail: true)
    }

// MARK: - Public closure
    var retrievedStoot: (() -> Void)?
    
// MARK: - Private vars
    private let stootId: Int
    private var stoot: Stoot?
    
    init(withStootId stootId: Int) {
        self.stootId = stootId
    }
 
    
// MARK: - Public method
    func getStootDetails() {
        DataManager.getStoot(withId: stootId) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let stoot):
                self.stoot = stoot
                self.retrievedStoot?()
            case .failure: break
//                If i had more time i would have implemented the error part with alertViewControllers
            }
        }
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        guard let questions = questions,
            questions.count > 0,
            let stootDetailSection = StootDetailSection(rawValue: section)
        else { return 0 }
        return stootDetailSection == .questions ? questions.count : 1
    }
}
